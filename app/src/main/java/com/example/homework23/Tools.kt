package com.example.homework23

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import kotlinx.android.synthetic.main.dialog_layout.*
import kotlinx.android.synthetic.main.selector_layout.*

object Tools {
    fun initDialog(context: Context, title: String, desc: String) {
        val dialog = Dialog(context)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_layout)

        val params: WindowManager.LayoutParams = dialog.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as WindowManager.LayoutParams
        dialog.dialogTitle.text = title
        dialog.dialogDescription.text = desc
        dialog.okButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()

    }

    fun <T> startActivity(activity: Activity, cls: Class<T>, bundle: Bundle) {
        val intent = Intent(activity, cls)
        intent.putExtras(bundle)
        activity.startActivity(intent)
        activity.overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out)

    }

    fun <T> startActivityForResult(
        activity: Activity,
        cls: Class<T>,
        requestCode: Int,
        bundle: Bundle?
    ) {
        val intent = Intent(activity, cls)
        if(bundle != null)
            intent.putExtras(bundle)
        activity.startActivityForResult(intent, requestCode)
        activity.overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out)

    }


}