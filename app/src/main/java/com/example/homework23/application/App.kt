package com.example.homework23.application

import android.app.Application
import okhttp3.internal.Internal.instance

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance =this
    }

    private val INSTANCE: App by lazy{
        this
    }

    companion object{
        var instance: App? = null
    }

    fun getApiKey() = "AIzaSyBADWUmhO9XNVF_-qSZR6RQWcoHfSpAr6E"
}