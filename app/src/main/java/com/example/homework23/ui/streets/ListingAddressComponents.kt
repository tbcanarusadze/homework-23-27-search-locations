package com.example.homework23.ui.streets

import com.google.gson.annotations.SerializedName

class ListingAddressComponents {
    @SerializedName("long_name")
    var longName = ""
    @SerializedName("short_name")
    var shortName = ""
    lateinit var types: MutableList<String>

}