package com.example.homework23.ui.countries

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.homework23.R
import com.example.homework23.dataloader.CountriesData
import com.example.homework23.dataloader.CustomCallback
import com.example.homework23.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_choose_country.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.json.JSONObject

class ChooseCountryActivity : BaseActivity() {

    private lateinit var adapter: CountryRecyclerViewAdapter
    private val countries = mutableListOf<CountryModel.Result>()
    private var countryId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_country)
        attachToolbar()
        init()
    }

    private fun attachToolbar(){
        setSupportActionBar(toolbarLayout)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = getString(R.string.countries)
        doneButton.text = getString(R.string.finalize)
        doneButton.setOnClickListener {
            finalize()
        }
    }

    private fun init(){
        countries.addAll(intent?.extras!!.getParcelableArrayList("countries")!!)
        countryId = intent!!.extras!!.getString("countryId", "")
        intent!!.extras!!.clear()


        adapter = CountryRecyclerViewAdapter(countries)
        countryRecyclerView.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        countryRecyclerView.adapter = adapter
        countryRecyclerView.setHasFixedSize(true)
        countryRecyclerView.scrollToPosition(adapter.selectedPosition)

//        getCountries()

        (0 until countries.size).forEach{
            if(countryId == countries[it].iso2){
                adapter.selectedPosition = it
                return
            }
        }

    }

//    private fun getCountries() {
//        CountriesData.getRequest(CountriesData.REQUEST, object : CustomCallback {
//            override fun onFailure(response: String) {
//                Toast.makeText(
//                    applicationContext,
//                    "check your internet connection and try again!",
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
//
//            override fun onResponse(response: String) {
//                val json = JSONObject(response)
//                if (json.has("results")) {
//                    val results = json.getJSONArray("results")
//                    (0 until results.length()).forEach {
//                        val value = results.getJSONObject(it)
//                        val result = CountryModel.Result(value.getString("name"),value.getString("iso2"))
//                        countries.add(result)
//                    }
//                }
//                adapter.notifyDataSetChanged()
//            }
//        })
//    }

    private fun finalize(){
        val model = countries[adapter.selectedPosition]
        val intent = intent
        intent.putExtra("countryId", model.iso2)
        intent.putExtra("countryName", model.name)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}
