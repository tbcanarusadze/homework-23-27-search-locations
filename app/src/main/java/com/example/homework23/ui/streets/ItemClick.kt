package com.example.homework23.ui.streets

interface ItemClick {
    fun onClick(position: Int)
}