package com.example.homework23.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.homework23.R

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
    }

    private fun back(){
        super.onBackPressed()
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)
    }

    override fun onBackPressed() {
        back()
    }
}
