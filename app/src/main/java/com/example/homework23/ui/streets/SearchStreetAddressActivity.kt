package com.example.homework23.ui.streets

import android.app.Activity
import android.content.Intent
import android.icu.text.Transliterator
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log.d
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework23.R
import com.example.homework23.application.App
import com.example.homework23.dataloader.CustomCallback
import com.example.homework23.dataloader.AutoComplete
import com.example.homework23.ui.custom_address.CustomAddressActivity
import kotlinx.android.synthetic.main.activity_search_location.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.json.JSONObject

class SearchStreetAddressActivity : AppCompatActivity() {

    private lateinit var searchRecyclerViewAdapter: SearchRecyclerViewAdapter
    private var addresses = mutableListOf<SearchStreetModel>()
    private var countryId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_location)
        init()
    }

    private fun init() {
        setSupportActionBar(toolbarLayout)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = getString(R.string.search_address)
        doneButton.text = getString(R.string.add)


        searchRecyclerViewAdapter = SearchRecyclerViewAdapter(
            addresses, itemClick
        )
        countryId = intent!!.extras!!.getString("countryId", "")
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = searchRecyclerViewAdapter
        searchEditText.addTextChangedListener(textWatcher)

        doneButton.setOnClickListener {
            val intent = Intent(this, CustomAddressActivity::class.java)
            startActivity(intent)
        }
    }

    private fun getAddresses(input: String) {
        addresses.clear()
        searchRecyclerViewAdapter.notifyDataSetChanged()
        if(input.isEmpty())
            return
        val parameters = mutableMapOf<String, String>()
        parameters["input"] = input
        parameters["key"] = App.instance!!.getApiKey()
        parameters["language"] = "en"
        parameters["components"] = "country:$countryId"
        d("countryId address", countryId)
        AutoComplete.getRequest(
            AutoComplete.AUTOCOMPLETE, parameters, object :
                CustomCallback {
                override fun onFailure(response: String) {
                    Toast.makeText(applicationContext, "failure", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(response: String) {
                    val json = JSONObject(response)
                    if (json.has("predictions")) {
                        val predictions = json.getJSONArray("predictions")
                        d("on response", "yes")
                        (0 until predictions.length()).forEach {
                            val prediction = predictions.getJSONObject(it)
                            addresses.add(
                                SearchStreetModel(
                                    prediction.getString("description"),
                                    prediction.getString("place_id")
                                )
                            )
                        }
                    }
                    searchRecyclerViewAdapter.notifyDataSetChanged()
                }

            })

    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            getAddresses(s.toString())
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            super.onBackPressed()
        return true
    }

    private val itemClick = object : ItemClick {
        override fun onClick(position: Int) {
            val intent = intent
            intent.putExtra("streetAddress", addresses[position].description)
            intent.putExtra("placeId", addresses[position].placeId)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}
