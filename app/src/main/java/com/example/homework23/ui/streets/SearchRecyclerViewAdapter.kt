package com.example.homework23.ui.streets

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.homework23.R
import kotlinx.android.synthetic.main.item_recyclerview_layout.view.*

class SearchRecyclerViewAdapter(private val items: MutableList<SearchStreetModel>, private val itemClick: ItemClick) :
    RecyclerView.Adapter<SearchRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size

    private lateinit var model: SearchStreetModel

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener{
        fun onBind() {
            model = items[adapterPosition]
            itemView.locationTextView.text = model.description
            itemView.setOnClickListener  (this)
        }

        override fun onClick(v: View?) {
            itemClick.onClick(adapterPosition)
        }

    }

}