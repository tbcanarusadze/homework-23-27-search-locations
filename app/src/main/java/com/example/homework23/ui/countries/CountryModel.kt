package com.example.homework23.ui.countries

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.text.ParseException

//class CountryModel{
//
//    lateinit var results: MutableList<Result>
//
//    class Result{
//        var name = ""
//        var iso2 = ""
//    }
//}
@Parcelize
data class CountryModel(var results: MutableList<Result>): Parcelable{
    @Parcelize
     data class Result(
        var name: String,
        var iso2: String
    ): Parcelable
}

