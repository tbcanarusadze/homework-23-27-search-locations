package com.example.homework23.ui.custom_address

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.homework23.R
import com.example.homework23.Tools
import com.example.homework23.dataloader.AutoComplete
import com.example.homework23.dataloader.CountriesData
import com.example.homework23.dataloader.CustomCallback
import com.example.homework23.ui.countries.ChooseCountryActivity
import com.example.homework23.ui.countries.CountryModel
import com.example.homework23.ui.streets.SearchStreetAddressActivity
import com.example.homework23.dataloader.StreetsCallback
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_custom_address.*
import kotlinx.android.synthetic.main.selector_layout.view.*
import kotlinx.android.synthetic.main.toolbar_layout.*


class CustomAddressActivity : AppCompatActivity() {

    companion object {
        const val CHOOSE_COUNTRY = 1
        const val CHOOSE_STREET_ADDRESS = 2
    }

    private var countryId = ""
    private val countries = mutableListOf<CountryModel.Result>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_address)
        attachToolbar()
        init()
        getCountries()
    }

    private fun attachToolbar() {
        setSupportActionBar(toolbarLayout)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = getString(R.string.address)
    }

    private fun init() {
        nextButton.setOnClickListener {
            next()
        }
        country_layout.setOnClickListener {
            chooseCountry()
        }
        street_layout.setOnClickListener {
            chooseAddress()
        }
        country_layout.titleTextView.text = getString(R.string.country_region)
        street_layout.titleTextView.text = getString(R.string.street_address)


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            super.onBackPressed()
        return true
    }

    fun next() {
        Tools.initDialog(
            this,
            getString(R.string.incorrect_request),
            getString(R.string.fill_all_fields)
        )
    }


    private fun chooseCountry() {
        val bundle = Bundle()
        bundle.putParcelableArrayList("countries", countries as ArrayList<CountryModel.Result>)
        bundle.putString("countryId", countryId)
        Tools.startActivityForResult(
            this,
            ChooseCountryActivity::class.java,
            CHOOSE_COUNTRY,
            bundle
        )
    }

    private fun chooseAddress() {
        val bundle = Bundle()
        bundle.putString("countryId", countryId)
        Tools.startActivityForResult(
            this,
            SearchStreetAddressActivity::class.java,
            CHOOSE_STREET_ADDRESS,
            bundle
        )

    }

    private fun getAddressDetail(placeId: String, streetAddress: String) {
        street_layout.contentTextView.text = streetAddress
        if(placeId.isNotEmpty()){
            AutoComplete.getAddressDetail(placeId, object :
                StreetsCallback<MutableMap<String, String>> {
                override fun doneResults(results: MutableMap<String, String>) {
                    if (results.containsKey("city"))
                        cityEditText.setText(results["city"])
                    if (results.containsKey("state"))
                        stateEditText.setText(results["state"])
                    if (results.containsKey("zipcode"))
                        zipCodeEditText.setText(results["zipcode"])
                }
            })
        }
    }

    private fun getCountries(){
        CountriesData.getRequest(CountriesData.REQUEST, object: CustomCallback{
            override fun onFailure(response: String) {

            }

            override fun onResponse(response: String) {
                countries.addAll(Gson().fromJson(response, CountryModel::class.java).results)
                if(countries.isNotEmpty())
                    setCountry(countries[0].iso2, countries[0].name)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHOOSE_COUNTRY) {
                setCountry(
                    data!!.extras!!.getString("countryId", ""),
                    data.extras!!.getString("countryName", "")
                )
            } else if (requestCode == CHOOSE_STREET_ADDRESS) {
                getAddressDetail(
                    data!!.extras!!.getString("placeId", ""),
                    data.extras!!.getString("streetAddress", "")
                )
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setCountry(countryId: String, countryName: String) {
        country_layout.contentTextView.text = countryName
        this.countryId = countryId
    }


}
