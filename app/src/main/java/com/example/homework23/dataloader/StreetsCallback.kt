package com.example.homework23.dataloader

interface StreetsCallback<T> {
    fun doneResults(results: MutableMap<String, String>)
}