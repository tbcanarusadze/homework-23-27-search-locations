package com.example.homework23.dataloader

interface CustomCallback {
    fun onFailure(response: String)
    fun onResponse(response: String)
}

//interface CustomCallback {
//    fun onFailure(response: String)
//    fun onResponse(response: String)
//    fun doneResults(results: MutableMap<String, String>){}
//}